using Microsoft.AspNetCore.Mvc;

namespace WebApp.Controllers;

[ApiController]
[Route("[controller]")]
public class WebAppController : ControllerBase
{
    // TODO: reproduce
    // https://jsonplaceholder.typicode.com/users

    public record User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string? Email { get; set; }

        public int? CompanyId { get; set; }
        public Company? Company { get; set; }
    }

    public record Company
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
