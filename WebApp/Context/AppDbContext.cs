﻿using Microsoft.EntityFrameworkCore;
using static WebApp.Controllers.WebAppController;

namespace WebApp.Context;

public class AppDbContext : DbContext
{
    public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
    {
        Database.EnsureCreated();
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseInMemoryDatabase(databaseName: "db");

        base.OnConfiguring(optionsBuilder);
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        // user 1
        var company = new Company
        {
            Id = 1,
            Name = "My Company"
        };

        modelBuilder.Entity<Company>().HasData(company);

        var user1 = new User
        {
            Id = 1,
            Name = "Tom",
            CompanyId = company.Id
        };

        modelBuilder.Entity<User>().HasData(user1);

        // user 2
        var user2 = new User
        {
            Id = 2,
            Name = "Alice"
        };

        modelBuilder.Entity<User>().HasData(user2);
    }
}