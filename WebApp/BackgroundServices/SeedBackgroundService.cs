﻿
namespace WebApp.BackgroundServices;

public class SeedBackgroundService : BackgroundService
{
    protected override Task ExecuteAsync(CancellationToken stoppingToken)
    {
        // HTTP GET https://jsonplaceholder.typicode.com/users
        // Seed database

        return Task.CompletedTask;
    }
}
